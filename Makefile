NAME = gopher
CC = clang -g
LD = clang -g

CFLAGS = -Wall -Werror -Wextra -pedantic -std=c11 \
 -fsanitize=undefined-trap -fsanitize-undefined-trap-on-error \
 -I/usr/local/include -I./include/

LDFLAGS = -L/usr/local/lib

NAMES = $(shell ls src | sed 's/\.c//')
OBJS = $(NAMES:%=obj/%.o)

all: $(NAME)

$(NAME): $(OBJS)
	$(LD) $(LDFLAGS) $(OBJS) -o $(NAME)

obj/%.o: src/%.c include/gopher.h
	mkdir -p obj
	$(CC) $(CFLAGS) -c $< -o $@

TEST = -s "example.com" \
	-p 71 \
	-i Makefile \
	-f "gopher=gopher.8" \
	-f "gopher/readme=README" \
	-d "src=src" \
	-d "src/include=include"
run: $(NAME)
	./$(NAME) $(TEST)

val: $(NAME)
	valgrind ./$(NAME) $(TEST)

readme: gopher.8
	mandoc -Tmarkdown gopher.8 \
	| sed -e 's/&nbsp;/ /g' -e 's/&#160;/ /g' \
		-e 's/&gt;/>/g' -e 's/&lt;/</g' \
	> README

clean:
	@rm -f $(NAME) a.out *.core obj/*.o vgcore.*

depend:
	$(CC) -E -MM $(CSRC) > .depend

