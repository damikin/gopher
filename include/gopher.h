/*
 * Copyright 2019 Chris Sonnenberg <damikin11@gmail.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GOPHER_H_
#define GOPHER_H_

#define BUF_SIZE 1024

typedef struct gfile {
	char *name;
	char *path;
	struct gfile *next;
} gfile_t;

typedef struct env {
	gfile_t *files;
	gfile_t *dirs;
	gfile_t *index;
	char *server;
} env_t;

env_t *init(int argc, char *argv[]);
char *handle_input(int fd);

int is_text(const char *path);
int is_directory(const char *path);
char *valid_path(const char *path, const char *name, const char *input);

void serve_directory(int out_fd,
	const char *path, const char *prefix, const char *server);
void serve_file(int out_fd, const char *path, int is_text);

void free_env(env_t *env);

#endif

