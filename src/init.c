/*
 * Copyright 2019 Chris Sonnenberg <damikin11@gmail.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include "gopher.h"

static void
usage() {
	errx(1, "usage: "
		"[-s serverdomain] "
		"[-p port] "
		"[-i file_or_directory] "
		"[-f servename=file] "
		"[-d servename=directory]");
}

env_t *
init(int argc, char *argv[]) {
	int c;
	char *i;
	char *server = "localhost";
	int port = 70;
	gfile_t *file = NULL;
	env_t *env = calloc(1, sizeof (*env));
	if(env == NULL)
		err(1, "init calloc");

	while((c = getopt(argc, argv, "s:p:i:f:d:")) != -1) {
		switch(c) {
		case 's':
			if(*optarg == '\0')
				usage();
			server = optarg;
			break;
		case 'p':
			if(sscanf(optarg, "%d", &port) != 1)
				usage();
			break;
		case 'i':
		case 'd':
		case 'f':
			if(c == 'i' && env->index != NULL)
				usage();

			file = calloc(1, sizeof (*file));
			if(file == NULL)
				err(1, "init file calloc");

			i = optarg;
			if(c != 'i') {
				file->name = i;
				i = index(optarg, '=');
				if(i == NULL || optarg == i)
					usage();
				*i = '\0';
				i++;
			}

			file->path = i;
			if(*i == '\0')
				usage();

			if(c == 'f') {
				file->next = env->files;
				env->files = file;
			} else if(c == 'd') {
				file->next = env->dirs;
				env->dirs = file;
			} else if(c == 'i') {
				if(env->index != NULL)
					errx(1, "init too many indexes");
				env->index = file;
			}
			file = NULL;
			break;
		default:
			usage();
		}
	}

	if(env->index == NULL
		&& env->files == NULL
		&& env->dirs == NULL)
		errx(1, "init nothing to serve");

	argc -= optind;
	if(argc > 0)
		usage();

	if(asprintf(&(env->server), "%s\t%d", server, port) == -1)
		err(1, "init asprintf");

	return env;
}

