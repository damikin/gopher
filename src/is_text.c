/*
 * Copyright 2019 Chris Sonnenberg <damikin11@gmail.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "gopher.h"

int
is_text(const char *path) {
	if(path == NULL)
		errx(1, "is_text path null");

	if(access(path, R_OK) == -1)
		err(1, "is_text open");

	char *cmd = NULL;
	if(asprintf(&cmd, "file -i %s", path) == -1)
		err(1, "is_text asprintf");

	char *buf = calloc(BUF_SIZE+1, sizeof (*buf));
	if(buf == NULL)
		err(1, "is_text calloc");

	FILE *f = popen(cmd, "r");
	if(f == NULL)
		errx(1, "is_text popen");

	ssize_t r = read(fileno(f), buf, BUF_SIZE);
	if(r == 0)
		errx(1, "is_text read nothing");
	else if(r == -1)
		err(1, "is_text read");

	char *p = strstr(buf, "text/");
	int ret = p != NULL;

	pclose(f);
	free(cmd);
	free(buf);
	return ret;
}

