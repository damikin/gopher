/*
 * Copyright 2019 Chris Sonnenberg <damikin11@gmail.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/stat.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "gopher.h"

int
main(int argc, char *argv[]) {
	int out_fd = 0;
	int in_fd = 1;

	env_t *env = init(argc, argv);
	if(env == NULL)
		errx(1, "env null");

	char *input = handle_input(in_fd);
	if(input == NULL)
		errx(1, "input null");

	char *path = NULL, *name = NULL;
	// serve index
	if(input[0] == '\0') {
		if(env->index == NULL)
			errx(1, "index null");
		path = valid_path(env->index->path, NULL, NULL);
		name = env->index->name;
		goto serve;
	}

	gfile_t *f = env->files;
	// serve files
	while(f != NULL) {
		if(strcmp(input, f->name) == 0) {
			path = valid_path(f->path, NULL, NULL);
			name = f->name;
			goto serve;
		}
		f = f->next;
	}

	f = env->dirs;
	size_t f_len = 0;
	// serve directories, last longest match
	while(f != NULL) {
		size_t len = strlen(f->name);
		if(f_len <= len && strncmp(input, f->name, len) == 0) {
			f_len = len;
			path = f->path;
			name = f->name;
		}
		f = f->next;
	}
	if(path != NULL) {
		path = valid_path(path, name, input);
		name = input;
		goto serve;
	}

	// maybe the index is a directory?
	if(env->index != NULL && is_directory(env->index->path)) {
		path = valid_path(env->index->path, "", input);
		name = input;
	}

serve:
	if(path == NULL)
		errx(1, "nothing to serve");

	if(is_directory(path))
		serve_directory(out_fd, path, name, env->server);
	else
		serve_file(out_fd, path, is_text(path));

	if(close(out_fd) == -1)
		err(1, "close");

	free(path);
	free(input);
	free_env(env);

	return 0;
}

