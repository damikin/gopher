/*
 * Copyright 2019 Chris Sonnenberg <damikin11@gmail.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <dirent.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "gopher.h"

void
serve_directory(int out_fd,
	const char *path, const char *prefix, const char *server) {

	if(out_fd < 0 || path == NULL || server == NULL)
		errx(1, "serve_directory arguments invalid");

	DIR *dir = opendir(path);
	if(dir == NULL)
		err(1, "serve_directory fdopendir");

	struct dirent *de = NULL;
	while((de = readdir(dir)) != NULL) {
		// skip dot files and directories
		if(de->d_name[0] == '.')
			continue;

		char type = 'i';
		switch(de->d_type) {
		case DT_REG: {
				char *fullname = NULL;
				if(asprintf(&fullname, "%s/%s", path, de->d_name) == -1)
					err(1, "serve_directory asprintf");
				if(is_text(fullname))
					type = '0';
				else
					type = '9';
				free(fullname);
			}
			break;
		case DT_DIR:
			type = '1';
			break;
		}

		if(prefix == NULL || prefix[0] == '\0') {
			if(dprintf(out_fd, "%c%s\t%s\t%s\r\n",
				type, de->d_name, de->d_name, server) < 0)
					err(1, "serve_directory dprintf");
		} else {
			if(dprintf(out_fd, "%c%s\t%s/%s\t%s\r\n",
				type, de->d_name, prefix, de->d_name, server) < 0)
					err(1, "serve_directory dprintf");
		}
	}

	if(dprintf(out_fd, ".\r\n") < 0)
		err(1, "serve_directory dprintf");

	if(closedir(dir) == -1)
		err(1, "serve_directory fdclosedir");
}

