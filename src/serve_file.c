/*
 * Copyright 2019 Chris Sonnenberg <damikin11@gmail.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "gopher.h"

void
serve_file(int out_fd, const char *path, int is_text) {
	if(out_fd < 0 || path == NULL)
		errx(1, "serve_file invalid arguments");

	int fd = open(path, O_RDONLY);
	if(fd == -1)
		err(1, "serve_file open");

	off_t size = lseek(fd, 0, SEEK_END);
	if(size == -1)
		err(1, "serve_file lseek end");

	if(lseek(fd, 0, SEEK_SET) == -1)
		err(1, "serve_file lseek beginning");

	uint8_t *buf = calloc(size, sizeof (*buf));
	if(buf == NULL)
		err(1, "serve_file calloc");

	ssize_t r = read(fd, buf, size);
	if(r == 0)
		errx(1, "serve_file read nothing");
	else if(r == -1)
		err(1, "serve_file read");

	ssize_t w = write(out_fd, buf, r);
	if(w == 0)
		errx(1, "serve_file write nothing");
	else if(w == -1)
		err(1, "serve_file write");

	if(is_text && dprintf(out_fd, "\r\n.\r\n") < 0)
		err(1, "serve_file dprintf");

	free(buf);
}

